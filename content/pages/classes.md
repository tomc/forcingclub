Title: Classes	
Date: 2019-11-25 17:04

No public classes are currently scheduled. The next public class is estimated to be in January/February of 2020 at [Eastside Bridge Center](https://www.bridgewebs.com/eastside/) in Redmond, WA.

For private classes, please contact Tom for more information.

----

### Recent Classes:
* Tools for Better Bidding (Sep/Oct 2019)
* Improve Your Defense (Apr/May 2019)
* Card Play Adventures (Oct/Nov 2018)
* Tune Your Bidding (Aug/Sep 2018)